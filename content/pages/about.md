+++
title = "about me"
path = "about"
+++

This is blog of abspython, A.K.A _Abhijith Sheheer_. Currently enrolled in B.Tech at Vidya Academy of Science and Technology, Thrissur.

You can find me at: <br>
Mastodon : [https://mastodon.social/@abhijithsheheer](https://mastodon.social/@abhijithsheheer) <br>
Telegram : [https://t.me/abspython](https://t.me/abspython) <br>

The most prefferable way to communicate with me is via mail.
``YWJoaWppdGhzaGVoZWVyQGdtYWlsLmNvbQ==``

Decode the above Base64 string to get my mail. Had to give hard time for spammers.😁
