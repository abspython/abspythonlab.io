+++
title = "Debian Packaging Part-1"
date = 2019-11-27


[taxonomies]
tags = ["Debian","Packaging"]
+++
This is going to a series of blog posts for helping others and _myself_ with the process of packaging software in Debian.

So, what is a **package**?

> "A Debian package is a collection of files that allow
> for applications or libraries to be distributed via the Debian package
> management system. The aim of packaging is to allow the automation of
> installing, upgrading, configuring, and removing computer programs for
> Debian in a consistent manner. "
> --- Debian Wiki

<!-- more -->

For packaging software, it is recommended to use the Debian Sid as it contains the latest software dependencies. If you have a Debian-Sid Machine you are good to go. If not we need to set up a packaging environment. There are many virtualization software in Linux like,
- Docker
- LXC
- VM (Using vagrant or manually installed)

_**P.S:** For beginners it is advised to use Debian Sid along with your current Operating System (Dual Booting) for the sake of avoiding complex issues. Or you could do containers if you know about or as last resort._

### Installing Packaging Tools

Now, after setting up our Debian-Sid, we need to install tools for helping us in packaging.

| Packaging Tool |                     man                      |
| :------------: | :------------------------------------------: |
|    gem2deb     |           For packaging ruby gems            |
|    npm2deb     |        For packaging Node.js modules         |
|    dh-make     | Generic tool; if no specific tool for a lang |

If the packaging tools are not installed, then install them by,

```bash
$ sudo apt-get install gem2deb npm2deb dh-make
$ npm install npm@latest -g # update npm
```

We need to add our email and name to `.bashrc` as the packaging tool will fill it out. Replace the email and name in the below code and write into `~/.bashrc`

```bash
export DEBEMAIL=youremail@domain
export DEBFULLNAME='Your Name'
alias lintian='lintian -iIEcv --pedantic --color auto'
alias git-import-dsc='git-import-dsc --author-is-committer --pristine-tar'
alias clean='fakeroot debian/rules clean'
```

Activate the changes in `~/.bashrc` by doing `source ~/.bashrc`.

🎉 Congrats yourself; you now have a working environment for the packaging.
The next part deals with the actual process of packaging.

`:wq` for today.
