+++
title = "How to upgrade an Debian Package? Part-1"
date = 2019-11-30

[taxonomies]
tags = ["Debian","Packaging"]

+++

This time we are going through an introduction how to  **upgrade** an existing packages in Debian. There are basically seven steps in this process.

<!-- more -->

_P.S: You can find details regarding the new terms in the footnote._<br><br>

1. We clone the packages repository in the [salsa](https://salsa.debian.org/)[^1] by using ``gbp clone <url>``[^2]. _**Note**: If you are using ``git clone``[^3], it will create``master`` branch only, you should create ``upstream`` and ``pristine-tar`` branches locally by running ``git checkout upstream; git checkout pristine-tar; git checkout master``._
2. We now need to get the new upstream version by either using ``uscan --verbose``[^4] or by [manually downloading via git snapshots](https://wiki.debian.org/Javascript/Nodejs/Npm2Deb#Option_2:_Download_via_github.com_commit_snapshot).
3. Import the new upstream version by ``gbp import-orig --pristine-tar ../<upstream-version.orig.tar.gz>``.
4. Add a new section in ``debian/changelog`` by ``gbp dch -a``[^5] (Not ``dch -a``).
5. Build the new upgraded package by ``dpkg buildpackage``[^6].
6. Fix the ``lintain``[^7] errors.
7. Build the package in a clean chroot(using tools like ``sbuild``[^8],etc.)

<br>

### **#Footnotes**


[^1]: [``salsa``](https://wiki.debian.org/Salsa) is a collaborative development server for Debian based on the GitLab software.

[^2]: [``gbp``](https://honk.sigxcpu.org/piki/projects/git-buildpackage/) is a suite to help with maintaining Debian packages in Git repositories.

[^3]: [``git``]((http://git-scm.com)) is a Version-Control system.

[^4]: [``uscan``]() is used to download the new.

[^5]:[``dch``](https://www.commandlinux.com/man-page/man1/dch.1.html) will add a new comment line to the Debian changelog in the current source tree.

[^6]:It is a control script that can be used to construct the package automatically. Also ``dpkg`` is the base of package management system in Debian.

[^7]: ``lintian`` dissects Debian packages and emits information about bugs and policy violations. It contains automated checks for many aspects of Debian policy as well as some checks for common errors.

[^8]: ``sbuild`` is used to test that their package builds in a minimal installation of Debian Unstable.  In particular, this helps ensure that you haven't missed any build dependencies.



<br>

This is how we upgrade existing packages in Debian. In next part, we are doing to try upgrading a package and go in more depth on the process of upgrading.

``:wq`` for today.