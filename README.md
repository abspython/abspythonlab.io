# Terminimal-Light

This is a fork of
[Terminimal](https://github.com/pawroman/zola-theme-terminimal), a Zola theme
forked of "Terminal" Hugo theme by Radosław Kozieł (aka. panr):
https://github.com/panr/hugo-theme-terminal
